<?php
header('Content-Type: application/json');
$host = "127.0.0.1";
$db = "aslab";
$user = "root";
$password = "";

$options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false
];

$dsn = "mysql:host=$host;dbname=$db;charset=utf8";

try {
    $pdo = new PDO($dsn, $user, $password, $options);
} catch(\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int) $e->getCode());
}
