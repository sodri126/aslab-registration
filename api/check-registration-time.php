<?php
include "configuration.php";
try {
    $stmt = $pdo->prepare("SELECT * FROM regist_time WHERE NOW() >= open_date AND NOW() <= close_date");
    $stmt->execute();

    $regist_time = $stmt->fetch();
    if ($regist_time) {
        echo json_encode([
            "code" => "success",
            "data" => $regist_time,
            "message" => "Pendaftaran masih dibuka!"
        ]);
    } else {
        echo json_encode([
            "code" => "failed",
            "message" => "Pendaftaran sudah ditutup, silahkan menunggu registrasti waktu dibuka!"
        ]);
    }
} catch(\Exception $e) {
    echo json_encode([
        "code" => "error",
        "message" => $e->getMessage()
    ]);
}

