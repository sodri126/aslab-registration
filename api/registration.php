<?php
include "configuration.php";
try {
    if (isset($_POST)) {
        $nim = $_POST['nim'];
        $nama = $_POST['nama'];
        $gender = $_POST['gender'];
        $year = $_POST['year'];
        $major = $_POST['major'];
        $email = $_POST['email'];
        $phone_number = $_POST['phone_number'];
        $stmt = $pdo->prepare("INSERT INTO `applicant` VALUES (?,?,?,?,?,?,?)");
        $data = [
            $nim,
            $nama,
            $gender,
            $year,
            $major,
            $email,
            $phone_number
        ];
        $insert = $stmt->execute($data);
        if ($insert) {
            echo json_encode([
                "code" => "success",
                "message" => "Data telah berhasil dimasukan!"
            ]);
        } else {
            echo json_encode([
                "code" => "failed",
                "message" => "Data gagal dimasukan!"
            ]);
        }
    } else {
        echo json_encode([
            "code" => "error",
            "message" => "Tidak melakukan pengiriman data!"
        ]);
    }
} catch(\Exception $e) {
    echo json_encode([
        "code" => "error",
        "message" => $e->getMessage()
    ]);
}

