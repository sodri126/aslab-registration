-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Des 2018 pada 10.37
-- Versi server: 10.1.32-MariaDB
-- Versi PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aslab`
--
CREATE DATABASE IF NOT EXISTS `pdsinerg` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pdsinerg`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `applicant`
--

CREATE TABLE `applicant` (
  `nim` varchar(12) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `gender` enum('Pria','Wanita') NOT NULL,
  `year` varchar(4) NOT NULL,
  `major` enum('Teknik Informatika','Sistem Informasi') NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone_number` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `applicant`
--

INSERT INTO `applicant` (`nim`, `nama`, `gender`, `year`, `major`, `email`, `phone_number`) VALUES
('41516010007', 'Leonardo', '', '2016', 'Sistem Informasi', 'g_banget@gmail.com', '089654512365'),
('41516010152', 'Muhamad shodri', '', '2016', 'Teknik Informatika', 'mshodri24@gmail.com', '082112062965'),
('41516010153', 'Jordi Amarudin', '', '2016', 'Teknik Informatika', 'jordi_amarudin@gmail.com', '081211112222');

-- --------------------------------------------------------

--
-- Struktur dari tabel `apptest`
--

CREATE TABLE `apptest` (
  `apptest_id` int(11) NOT NULL,
  `register_history_id` varchar(80) NOT NULL,
  `score` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `apptest`
--

INSERT INTO `apptest` (`apptest_id`, `register_history_id`, `score`) VALUES
(1, 'REG/BATCH-1/41516010152', 80),
(2, 'REG/BATCH-1/41516010153', 65);

-- --------------------------------------------------------

--
-- Struktur dari tabel `register_history`
--

CREATE TABLE `register_history` (
  `register_history_id` varchar(80) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `regist_time_id` int(11) NOT NULL,
  `nim` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `register_history`
--

INSERT INTO `register_history` (`register_history_id`, `schedule_id`, `regist_time_id`, `nim`) VALUES
('REG/BATCH-1/41516010152', 1, 1, '41516010152'),
('REG/BATCH-1/41516010153', 1, 1, '41516010153');

-- --------------------------------------------------------

--
-- Struktur dari tabel `regist_time`
--

CREATE TABLE `regist_time` (
  `regist_time_id` int(11) NOT NULL,
  `open_date` date NOT NULL,
  `close_date` date NOT NULL,
  `description` varchar(255) NOT NULL,
  `max_regist` int(7) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `regist_time`
--

INSERT INTO `regist_time` (`regist_time_id`, `open_date`, `close_date`, `description`, `max_regist`) VALUES
(1, '2018-12-13', '2018-12-15', 'Registration Batch 1', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `schedule`
--

CREATE TABLE `schedule` (
  `schedule_id` int(11) NOT NULL,
  `regist_time_id` int(11) NOT NULL,
  `test_datetime` datetime NOT NULL,
  `place` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `schedule`
--

INSERT INTO `schedule` (`schedule_id`, `regist_time_id`, `test_datetime`, `place`) VALUES
(1, 1, '2018-12-26 07:00:00', 'T-005'),
(2, 1, '2018-12-31 13:00:00', 'T-301');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `applicant`
--
ALTER TABLE `applicant`
  ADD PRIMARY KEY (`nim`);

--
-- Indeks untuk tabel `apptest`
--
ALTER TABLE `apptest`
  ADD PRIMARY KEY (`apptest_id`),
  ADD KEY `fk_apptest_register_history1_idx` (`register_history_id`);

--
-- Indeks untuk tabel `register_history`
--
ALTER TABLE `register_history`
  ADD PRIMARY KEY (`register_history_id`),
  ADD KEY `fk_register_history_applicant1_idx` (`nim`),
  ADD KEY `fk_register_history_regist_time1_idx` (`regist_time_id`),
  ADD KEY `fk_register_history_schedule1_idx` (`schedule_id`);

--
-- Indeks untuk tabel `regist_time`
--
ALTER TABLE `regist_time`
  ADD PRIMARY KEY (`regist_time_id`);

--
-- Indeks untuk tabel `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`schedule_id`),
  ADD KEY `fk_schedule_regist_time1_idx` (`regist_time_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `apptest`
--
ALTER TABLE `apptest`
  MODIFY `apptest_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `regist_time`
--
ALTER TABLE `regist_time`
  MODIFY `regist_time_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `schedule`
--
ALTER TABLE `schedule`
  MODIFY `schedule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `apptest`
--
ALTER TABLE `apptest`
  ADD CONSTRAINT `fk_apptest_register_history1` FOREIGN KEY (`register_history_id`) REFERENCES `register_history` (`register_history_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `register_history`
--
ALTER TABLE `register_history`
  ADD CONSTRAINT `fk_register_history_applicant1` FOREIGN KEY (`nim`) REFERENCES `applicant` (`nim`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_register_history_regist_time1` FOREIGN KEY (`regist_time_id`) REFERENCES `regist_time` (`regist_time_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_register_history_schedule1` FOREIGN KEY (`schedule_id`) REFERENCES `schedule` (`schedule_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `schedule`
--
ALTER TABLE `schedule`
  ADD CONSTRAINT `fk_schedule_regist_time1` FOREIGN KEY (`regist_time_id`) REFERENCES `regist_time` (`regist_time_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
