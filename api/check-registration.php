<?php
include "configuration.php";
try {
    if (isset($_POST)) {
        $nim = $_POST['nim'];
        $stmt = $pdo->prepare("SELECT `a`.`nim` FROM register_history `a` INNER JOIN applicant `b`
                               ON `a`.`nim` = `b`.`nim` WHERE `a`.`nim` = ?");
        $stmt->execute([$nim]);

        $student = $stmt->fetch();
        if ($student) {
            echo json_encode([
                "code" => "success",
                "message" => "NIM ". $nim." sudah teregistrasi!"
            ]);
        } else {
            echo json_encode([
                "code" => "failed",
                "message" => "NIM ".$nim." belum diregistrasikan!"
            ]);
        }
    } else {
        echo json_encode([
            "code" => "error",
            "message" => "Tidak melakukan pengiriman data!"
        ]);
    }
} catch(\Exception $e) {
    echo json_encode([
        "code" => "error",
        "message" => $e->getMessage()
    ]);
}

