<?php
include "configuration.php";
try {
    if (isset($_POST)) {
        $registrationId = $_POST['registrationId'];
        $stmt = $pdo->prepare("SELECT `a`.nim, `c`.nama, `b`.score FROM register_history `a` INNER JOIN apptest `b` ON
                                `a`.register_history_id = `b`.register_history_id
                                INNER JOIN applicant `c` ON `a`.nim = `c`.nim
                                WHERE `a`.register_history_id = ?");

        $stmt->execute([$registrationId]);

        $announce = $stmt->fetch();
        if ($announce) {
            echo json_encode([
                "code" => "success",
                "data" => $announce,
                "message" => "The data has been found"
            ]);
        } else {
            echo json_encode([
                "code" => "failed",
                "message" => "No data"
            ]);
        }
    } else {
        echo json_encode([
            "code" => "error",
            "message" => "No data has been posted"
        ]);
    }
} catch(\Exception $e) {
    echo json_encode([
        "code" => "error",
        "message" => $e->getMessage()
    ]);
}

