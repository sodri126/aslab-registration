<?php
include "configuration.php";
try {
    if (isset($_POST)) {
        $registrationId = $_POST['registrationId'];
        $stmt = $pdo->prepare("SELECT `a`.*, (SELECT COUNT(c.schedule_id) FROM register_history c 
                            WHERE c.schedule_id = a.schedule_id) as total FROM schedule `a` 
                            INNER JOIN register_history `b` ON `a`.`schedule_id` = `b`.`schedule_id` 
                            WHERE `b`.`register_history_id` = ?");

        $stmt->execute([$registrationId]);

        $schedule = $stmt->fetch();
        if ($schedule) {
            echo json_encode([
                "code" => "success",
                "data" => $schedule,
                "message" => "Data telah ditemukan"
            ]);
        } else {
            echo json_encode([
                "code" => "failed",
                "message" => "Data tidak ada"
            ]);
        }
    } else {
        echo json_encode([
            "code" => "error",
            "message" => "Tidak melakukan pengiriman data!"
        ]);
    }
} catch(\Exception $e) {
    echo json_encode([
        "code" => "error",
        "message" => $e->getMessage()
    ]);
}

