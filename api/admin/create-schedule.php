<?php
include "../configuration.php";
try {
    $regist_time_id = $_POST['regist_time_id'];
    $tempat = $_POST['place'];
    $tanggal = $_POST['tanggal'];
    $jam = $_POST['jam'];

    $pdo->beginTransaction();
    $isInserted = true;
    for ($i = 0; $i < count($tempat); $i++) {
        $mergeDate = $tanggal[$i]." ".$jam[$i];
        $data = [
            $regist_time_id[$i],
            $mergeDate,
            $place[$i]
        ];
        $stmtSchedule = $pdo->prepare("INSERT INTO `schedule` VALUES (NULL,?,?,?);");
        $isInserted &=  $stmtSchedule->execute();
    } 

    if ($isInserted) {
        $pdo->commit();
        echo json_encode([
            "code" => "success",
            "message" => "Jadwal telah berhasil ditambah!"
        ]);
    } else {
        $pdo->rollBack();
        echo json_encode([
            "code" => "failed",
            "message" => "Jadwal tidak berhasil ditambah!"
        ]);
    }
} catch(\Exception $e) {
    echo json_encode([
        "code" => "error",
        "message" => $e->getMessage()
    ]);
}