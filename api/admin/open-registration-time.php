<?php
include "../configuration.php";
try {
    $open_date = $_POST['open_date'];
    $close_date = $_POST['close_date'];
    $description = $_POST['description'];
    $max_regists = $_POST['max_regist'];
    
    $pdo->beginTransaction();
    $stmt = $pdo->prepare("INSERT INTO `regist_time` VALUES (?,?,?,?);");
    $isInserted =  $stmt->execute();

    if ($isInserted) {
        $pdo->commit();
        echo json_encode([
            "code" => "success",
            "message" => "Registrasi telah dibuka!"
        ]);
    } else {
        $pdo->rollBack();
        echo json_encode([
            "code" => "failed",
            "message" => "Gagal membuka registrasi, silahkan dicoba lagi!"
        ]);
    }
} catch(\Exception $e) {
    echo json_encode([
        "code" => "error",
        "message" => $e->getMessage()
    ]);
}