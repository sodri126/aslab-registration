<?php
include "../configuration.php";
try {
    if (isset($_GET)) {
        $type = $_GET['type'];
        $page = $_GET['page'];
        $limit = $_GET['limit'];

        $page = $page * $limit;
        $limit = $page + $limit;

        $sql = "";
        if ($type == "upcoming") {
            $sql = "SELECT `a`.* FROM `schedule` `a` WHERE `a`.test_datetime > NOW() LIMIT $page, $limit";
        } else if ($type == "past") {
            $sql = "SELECT `a`.* FROM `schedule` `a` WHERE `a`.test_datetime < NOW() LIMIT $page, $limit";
        } else {
            $sql = "SELECT `a`.* FROM `regist_time` `a` LIMIT $page, $limit";
        }

        $stmt = $pdo->prepare($sql);
        
        $stmt->execute();

        $schedule = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($schedule) {
            echo json_encode([
                "code" => "success",
                "data" => $schedule,
                "message" => "Data ditemukan"
            ]);
        } else {
            echo json_encode([
                "code" => "failed",
                "message" => "Data tidak ditemukan"
            ]);
        }
    } else {
        echo json_encode([
            "code" => "error",
            "message" => "Tidak melakukan pengiriman data!"
        ]);
    }
} catch(\Exception $e) {
    echo json_encode([
        "code" => "error",
        "message" => $e->getMessage()
    ]);
}

