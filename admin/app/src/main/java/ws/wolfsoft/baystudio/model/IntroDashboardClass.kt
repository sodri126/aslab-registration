package ws.wolfsoft.baystudio.model

import ws.wolfsoft.baystudio.R

enum class IntroDashboardClass private constructor(private val mTitleResId: Int, private val mContentResId: Int, private val mIconResId: Int, private val mLayoutResId: Int) {

    FIRST(R.string.first_slide_title, R.string.first_slide_content, R.drawable.robot, R.layout.item_intro_slide_one),
    SECOND(R.string.second_slide_title, R.string.second_slide_content, R.drawable.robot, R.layout.item_intro_slide_second),
    THIRD(R.string.third_slide_title, R.string.third_slide_content, R.drawable.robot, R.layout.item_intro_slide_third);

    fun getmTitleResId(): Int {
        return mTitleResId
    }

    fun getmContentResId(): Int {
        return mContentResId
    }

    fun getmIconResId(): Int {
        return mIconResId
    }

    fun getmLayoutResId(): Int {
        return mLayoutResId
    }


}
