package ws.wolfsoft.baystudio.component

import com.google.gson.GsonBuilder
import io.reactivex.Single
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import ws.wolfsoft.baystudio.BuildConfig
import ws.wolfsoft.baystudio.model.CreateSchedule
import ws.wolfsoft.baystudio.model.NowPrevSchedule
import ws.wolfsoft.baystudio.model.RegistrationBatch
import ws.wolfsoft.baystudio.model.RegistrationTime
import java.net.CookieManager
import java.util.Date
import java.util.concurrent.TimeUnit

interface APIAslabRegistrationAdmin {
    @GET("future-previous-schedule.php")
    fun getScheduleNowUp(): Single<NowPrevSchedule>

    @POST("open-registration-time.php")
    @FormUrlEncoded
    fun openRegistrationTime(@Field("open_date") openDate: String,
                             @Field("close_date") closeDate: String,
                             @Field("description") description: String,
                             @Field("max_regist") maxRegist: Int,
                             @Field("place[]") place: Array<String>,
                             @Field("tanggal[]") tanggal: Array<String>,
                             @Field("jam[]") jam: Array<String>):Single<RegistrationTime>

    @GET("list-registration-batch.php")
    fun getListRegistrationBatch(): Single<RegistrationBatch>

    @POST("create-schedule.php")
    @FormUrlEncoded
    fun createSchedule(@Field("regist_time_id") registTimeId: String,
                       @Field("place[]") place: Array<String>,
                       @Field("tanggal[]") tanggal: Array<String>,
                       @Field("jam[]") jam: Array<String>) : Single<CreateSchedule>

    companion object Factory{
        fun create(): APIAslabRegistrationAdmin {
            val READ_TIMEOUT = 100 * 1000
            val CONNECTION_TIMEOUT = 100 * 1000

            val logger = HttpLoggingInterceptor()
            logger.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

            val okHttp =  OkHttpClient.Builder()
                    .cookieJar(JavaNetCookieJar(CookieManager()))
                    .retryOnConnectionFailure(false)
                    .readTimeout(READ_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
                    .connectTimeout(CONNECTION_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
                    .addInterceptor(logger)
                    .build()

            var gson = GsonBuilder()
                    .setDateFormat("yyyy-MM-dd HH:mm:ss")
                    .create()

            val retrofit = Retrofit.Builder()
                    .client(okHttp)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl("http://aslab.pdsinergi.com/api/admin/")
                    .build()

            return retrofit.create(APIAslabRegistrationAdmin::class.java)
        }
    }
}