package ws.wolfsoft.baystudio.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import io.github.pierry.progress.Progress
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

import java.util.ArrayList

import ws.wolfsoft.baystudio.activity.BuatJadwalActivity
import ws.wolfsoft.baystudio.activity.BukaRegistrasiActivity
import ws.wolfsoft.baystudio.R
import ws.wolfsoft.baystudio.activity.ReportActivity
import kotlinx.android.synthetic.main.fragment_home.*
import ws.wolfsoft.baystudio.activity.HomeActivity
import ws.wolfsoft.baystudio.model.NowPrevSchedule

class HomeFragment : Fragment() {
    private lateinit var getActivity: HomeActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getActivity = activity as HomeActivity
        allEventActivities()
        setupViewPager(viewPager)
    }

    private fun allEventActivities() {
        bukaReg.setOnClickListener {
            val a = Intent(activity, BukaRegistrasiActivity::class.java)
            startActivity(a)
        }
        buatKelas.setOnClickListener {
            val a = Intent(activity, BuatJadwalActivity::class.java)
            startActivity(a)
        }
        buatReport.setOnClickListener {
            val a = Intent(activity, ReportActivity::class.java)
            startActivity(a)
        }
        getScheduleNowUpcoming()
    }

    @SuppressLint("CheckResult")
    private fun getScheduleNowUpcoming() {
        if (getActivity.isNetworkConnected()) {
            val progressDialog = Progress(getActivity)
            progressDialog.light("Loading")
            getActivity.getAPI().getScheduleNowUp()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        nowPrevSchedule: NowPrevSchedule? ->
                        progressDialog.dismiss()
                        when (nowPrevSchedule?.code) {
                            "success" -> {
                                txt_schedule_now.text = nowPrevSchedule.data.now_schedule_date ?: "Tidak ada Jadwal"
                                txt_schedule_upcoming.text = nowPrevSchedule.data.future_schedule_date ?: "Tidak ada Jadwal"
                            }
                            "failed" -> Toast.makeText(getActivity, nowPrevSchedule.message, Toast.LENGTH_SHORT).show()

                            else -> {
                                Log.e("Error", nowPrevSchedule?.message)
                            }
                        }
                    }, {
                        error ->
                        progressDialog.dismiss()
                        Log.e("Error", error.message)
                    })
        }
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val viewPagerAdapter = ViewPagerAdapter(childFragmentManager)

        viewPagerAdapter.addFragment(FirstFragment(), "First")
        viewPagerAdapter.addFragment(SecoundFragment(), "Secound")
        viewPagerAdapter.addFragment(ThirdFragment(), "Third")

        viewPager.adapter = viewPagerAdapter
    }


    private inner class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
        internal var fragmentList: MutableList<Fragment> = ArrayList()
        internal var fragmentTitles: MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return fragmentTitles[position]
        }

        fun addFragment(fragment: Fragment, name: String) {
            fragmentList.add(fragment)
            fragmentTitles.add(name)
        }
    }

    class FirstFragment : Fragment() {

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
        }

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

            return inflater.inflate(R.layout.fragment_news_one, container, false)
        }

        companion object {
            fun newInstance(): FirstFragment {
                return FirstFragment()
            }
        }
    }

    class SecoundFragment : Fragment() {

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
        }

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

            return inflater.inflate(R.layout.fragment_news_two, container, false)
        }

        companion object {

            fun newInstance(): SecoundFragment {
                return SecoundFragment()
            }
        }

    }

    class ThirdFragment : Fragment() {

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
        }

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

            return inflater.inflate(R.layout.fragment_news_three, container, false)
        }

        companion object {
            fun newInstance(): ThirdFragment {
                return ThirdFragment()
            }
        }
    }

}