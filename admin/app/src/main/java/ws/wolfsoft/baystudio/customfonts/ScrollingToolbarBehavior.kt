package ws.wolfsoft.baystudio.customfonts

import android.content.Context
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CoordinatorLayout
import android.support.v7.widget.Toolbar
import android.util.AttributeSet
import android.view.View

/**
 * Created by Thanvandh on 09/07/2016.
 */
internal class ScrollingToolbarBehavior(context: Context, attrs: AttributeSet) : CoordinatorLayout.Behavior<Toolbar>(context, attrs) {

    override fun layoutDependsOn(parent: CoordinatorLayout, child: Toolbar, dependency: View): Boolean {
        return dependency is AppBarLayout
    }

    override fun onDependentViewChanged(parent: CoordinatorLayout, child: Toolbar, dependency: View): Boolean {
        if (dependency is AppBarLayout) {

            val distanceToScroll = child.height

            val bottomToolbarHeight = child.height//TODO replace this with bottom toolbar height.

            val ratio = dependency.getY() / bottomToolbarHeight.toFloat()
            child.translationY = -distanceToScroll * ratio
        }
        return true
    }
}
