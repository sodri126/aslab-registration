package ws.wolfsoft.baystudio.fragment

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import java.util.ArrayList

import ws.wolfsoft.baystudio.activity.BuatJadwalActivity
import ws.wolfsoft.baystudio.R
import ws.wolfsoft.baystudio.fragment.JadwalFragment.*

class JadwalFragment : Fragment() {

    lateinit var viewPager: ViewPager
    lateinit var buatJadwal: ImageView

    private var tabLayout: TabLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_jadwal, container, false)

        viewPager = view.findViewById(R.id.viewPager)
        tabLayout = view.findViewById(R.id.tab_layout)

        buatJadwal = view.findViewById(R.id.buatJadwal)
        buatJadwal.setOnClickListener {
            val a = Intent(activity, BuatJadwalActivity::class.java)
            startActivity(a)
        }

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupViewPager(viewPager)
        setupTabLayout(tabLayout!!)
    }

    private fun setupTabLayout(tabLayout: TabLayout) {
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        tabLayout.tabMode = TabLayout.MODE_FIXED
        tabLayout.addTab(tabLayout.newTab().setText("Upcoming"))
        tabLayout.addTab(tabLayout.newTab().setText("Past"))
        tabLayout.addTab(tabLayout.newTab().setText("Registrasi"))

        tabLayout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position

            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })


    }

    private fun setupViewPager(viewPager: ViewPager) {
        val viewPagerAdapter = ViewPagerAdapter(childFragmentManager)

        viewPagerAdapter.addFragment(JadwalUpcoming(), "Upcoming Jadwal")
        viewPagerAdapter.addFragment(JadwalPast(), "Past Jadwal")
        viewPagerAdapter.addFragment(Registrasi(), "Registrasi")

        viewPager.adapter = viewPagerAdapter
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
    }


    private inner class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
        internal var fragmentList: MutableList<Fragment> = ArrayList()
        internal var fragmentTitles: MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return fragmentTitles[position]
        }

        fun addFragment(fragment: Fragment, name: String) {
            fragmentList.add(fragment)
            fragmentTitles.add(name)
        }
    }

    class JadwalUpcoming : Fragment() {

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
        }

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

            return inflater.inflate(R.layout.fragment_jadwal_next, container, false)
        }

        companion object {
            fun newInstance(): JadwalUpcoming {
                return JadwalUpcoming()
            }
        }
    }

    class JadwalPast : Fragment() {

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
        }

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

            return inflater.inflate(R.layout.fragment_jadwal_after, container, false)
        }

        companion object {

            fun newInstance(): JadwalPast {
                return JadwalPast()
            }
        }

    }

    class Registrasi : Fragment() {

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
        }

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

            return inflater.inflate(R.layout.fragment_registrasi, container, false)
        }

        companion object {

            fun newInstance(): Registrasi {
                return Registrasi()
            }
        }

    }

    companion object {

        fun newInstance(): JadwalFragment {
            return JadwalFragment()
        }
    }

}