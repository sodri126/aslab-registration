package ws.wolfsoft.baystudio.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ws.wolfsoft.baystudio.R
import kotlinx.android.synthetic.main.item_list_schedule.view.*

class ScheduleAdapter(val context: Context, val items: ArrayList<ViewHolder>) : RecyclerView.Adapter<ScheduleAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, viewType: Int) {
        //holder.edit_text_tanggal_ujian_holder.setDateFormat(LazyDatePicker.DateFormat.valueOf("yyyy-mm-dd"))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_list_schedule, parent, false))
    }


    override fun getItemCount(): Int {
        return items.size
    }


    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val edit_text_place_holder = itemView.edit_text_place
        val edit_text_tanggal_ujian_holder = itemView.edit_text_tanggal_ujian
        val edit_text_jam_ujian_holder = itemView.edit_text_jam_ujian
    }
}