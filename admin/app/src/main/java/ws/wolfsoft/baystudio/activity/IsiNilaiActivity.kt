package ws.wolfsoft.baystudio.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View

import ws.wolfsoft.baystudio.R

class IsiNilaiActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_isi_nilai)
    }

    fun btnBack(view: View) {
        onBackPressed()
    }
}
