package ws.wolfsoft.baystudio.activity

import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_intro_screen.*

import ws.wolfsoft.aslabregistration.base.BaseActivity

import ws.wolfsoft.baystudio.R
import ws.wolfsoft.baystudio.adapter.IntroPagerAdapter

class IntroScreenActivity : BaseActivity() {
    override fun getLayoutId(): Int {
        return R.layout.activity_intro_screen
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        allEventsActivities()
    }

    private fun allEventsActivities() {
        bottom_btn.setOnClickListener {
            val i = Intent(this@IntroScreenActivity, HomeActivity::class.java)
            this@IntroScreenActivity.startActivity(i)
            finish()
        }

        viewPager.adapter = IntroPagerAdapter(this)
        indicator.setViewPager(viewPager)
    }




}
