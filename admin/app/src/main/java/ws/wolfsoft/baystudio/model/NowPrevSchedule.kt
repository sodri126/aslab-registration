package ws.wolfsoft.baystudio.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import ws.wolfsoft.aslabregistration.base.BaseResponse

class NowPrevSchedule : BaseResponse() {
    @Expose
    @SerializedName("data")
    lateinit var data: Data

    inner class Data {

        @Expose
        @SerializedName("future_schedule")
        var future_schedule_date: String? = null

        @Expose
        @SerializedName("now_schedule")
        var now_schedule_date: String? = null

    }
}