package ws.wolfsoft.baystudio.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import ws.wolfsoft.aslabregistration.base.BaseResponse
import java.util.*

class RegistrationBatch : BaseResponse() {
    @Expose
    @SerializedName("data")
    lateinit var data: List<Data>

    inner class Data {
        @Expose
        @SerializedName("regist_time_id")
        var regist_time_id: Int? = null

        @Expose
        @SerializedName("open_date")
        lateinit var open_date: Date

        @Expose
        @SerializedName("close_date")
        lateinit var close_date: Date

        @Expose
        @SerializedName("description")
        lateinit var description: String

        @Expose
        @SerializedName("max_regist")
        var max_regist: Int? = null

        override fun toString(): String {
            return this.description
        }
    }
}