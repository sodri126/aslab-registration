package ws.wolfsoft.aslabregistration.component

interface INavigator {
    fun backToNavigate()
}