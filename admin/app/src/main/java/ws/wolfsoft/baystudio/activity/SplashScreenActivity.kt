package ws.wolfsoft.baystudio.activity

import android.content.Intent
import android.os.Bundle
import ws.wolfsoft.aslabregistration.base.BaseActivity

import ws.wolfsoft.baystudio.R

class SplashScreenActivity : BaseActivity() {
    override fun getLayoutId(): Int {
        return R.layout.activity_splash_screen
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val background = object : Thread() {
            override fun run() {

                try {
                    // Thread will sleep for 5 seconds
                    Thread.sleep((2 * 1000).toLong())

                    val intent = Intent(this@SplashScreenActivity, IntroScreenActivity::class.java)
                    startActivity(intent)


                    // After 5 seconds redirect to another intent


                    //Remove activity
                    finish()

                } catch (e: Exception) {

                }

            }
        }

        // start thread
        background.start()

    }
}
