package ws.wolfsoft.aslabregistration.base

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import ws.wolfsoft.baystudio.component.APIAslabRegistrationAdmin

abstract class BaseActivity : AppCompatActivity() {

    @LayoutRes
    abstract fun getLayoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
    }

    fun isNetworkConnected() : Boolean {
        val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return if(networkInfo != null && networkInfo.isConnected) true
        else  {
            Toast.makeText(this, "Tidak ada koneksi internet!", Toast.LENGTH_SHORT).show()
            false
        }
    }

    fun getAPI(): APIAslabRegistrationAdmin {
        return APIAslabRegistrationAdmin.create()
    }


}