package ws.wolfsoft.baystudio.activity

import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.widget.Toast
import io.github.pierry.progress.Progress
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ws.wolfsoft.aslabregistration.base.BaseActivity

import ws.wolfsoft.baystudio.R
import ws.wolfsoft.baystudio.adapter.ScheduleAdapter
import kotlinx.android.synthetic.main.activity_buka_registrasi.*
import kotlinx.android.synthetic.main.item_list_schedule.view.*
import ws.wolfsoft.baystudio.model.RegistrationTime
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class BukaRegistrasiActivity : BaseActivity() {
    private var items: ArrayList<ScheduleAdapter.ViewHolder> = arrayListOf<ScheduleAdapter.ViewHolder>()

    override fun getLayoutId(): Int {
        return R.layout.activity_buka_registrasi
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        allEventsActivitiy()
    }

    private fun allEventsActivitiy() {
        rvJadwal.layoutManager = LinearLayoutManager(this)
        items.add(ScheduleAdapter.ViewHolder(LayoutInflater.from(this@BukaRegistrasiActivity).inflate(R.layout.item_list_schedule, rvJadwal, false)))
        rvJadwal.adapter = ScheduleAdapter(this, items)
        btn_back.setOnClickListener {
            onBackPressed()
        }


        btn_add.setOnClickListener {
            items.add(ScheduleAdapter.ViewHolder(LayoutInflater.from(this@BukaRegistrasiActivity).inflate(R.layout.item_list_schedule, rvJadwal, false)))
            (rvJadwal.adapter as ScheduleAdapter).notifyDataSetChanged()
        }

        edit_text_open_date.setOnClickListener{
            val calendar = Calendar.getInstance()
            val datePickerDialog = DatePickerDialog(this@BukaRegistrasiActivity, {
                view, year, monthOfYear, dayOfMonth ->
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                edit_text_open_date.setText(sdf.format(calendar.time))
            }, calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH))
            datePickerDialog.show()
        }

        edit_text_close_date.setOnClickListener {
            val calendar = Calendar.getInstance()
            DatePickerDialog(this@BukaRegistrasiActivity, {
                view, year, monthOfYear, dayOfMonth ->
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                edit_text_close_date.setText(sdf.format(calendar.time))
            }, calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)).show()
        }

        bottom_btn.setOnClickListener {
            if (isNetworkConnected()) {
                val progressDialog = Progress(this)
                val place: ArrayList<String> = arrayListOf()
                val tanggal: ArrayList<String> = arrayListOf()
                val jam: ArrayList<String> = arrayListOf()
                progressDialog.light("Loading")
                for (i in 0 until rvJadwal.childCount) {
                    place.add(rvJadwal.getChildAt(i).edit_text_place.text.toString())
                    tanggal.add(rvJadwal.getChildAt(i).edit_text_tanggal_ujian.text.toString())
                    jam.add(rvJadwal.getChildAt(i).edit_text_jam_ujian.text.toString())
                }
                getAPI().openRegistrationTime(edit_text_open_date.text.toString(),
                                            edit_text_close_date.text.toString(),
                                            edit_text_description.text.toString(),
                                            Integer.valueOf(edit_text_max_regist.text.toString()),
                                            place.toTypedArray(),
                                            tanggal.toTypedArray(),
                                            jam.toTypedArray())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            registrationTime: RegistrationTime? ->
                            progressDialog.dismiss()
                            when(registrationTime?.code) {
                                "success" -> {
                                    Toast.makeText(this@BukaRegistrasiActivity, registrationTime.message, Toast.LENGTH_SHORT)
                                    onBackPressed()
                                }
                                "failed" -> Toast.makeText(this@BukaRegistrasiActivity, registrationTime.message, Toast.LENGTH_SHORT).show()
                                else -> Log.e("Error", registrationTime?.message)
                            }
                        }, {
                            error ->
                            progressDialog.dismiss()
                            Log.e("Error", error.message)
                        })
            }
        }
    }

    private fun getAllRegistrationBatch() {

    }
}