package ws.wolfsoft.baystudio.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.widget.ArrayAdapter
import android.widget.Toast
import io.github.pierry.progress.Progress
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_buat_jadwal.*
import kotlinx.android.synthetic.main.item_list_schedule.view.*
import ws.wolfsoft.aslabregistration.base.BaseActivity

import ws.wolfsoft.baystudio.R
import ws.wolfsoft.baystudio.adapter.ScheduleAdapter
import ws.wolfsoft.baystudio.model.CreateSchedule
import ws.wolfsoft.baystudio.model.RegistrationBatch

class BuatJadwalActivity : BaseActivity() {
    private var items: ArrayList<ScheduleAdapter.ViewHolder> = arrayListOf<ScheduleAdapter.ViewHolder>()

    override fun getLayoutId(): Int {
        return R.layout.activity_buat_jadwal
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        allEventsActivity()
        getRegistrationBatch()
    }

    private fun allEventsActivity() {
        rvJadwal.layoutManager = LinearLayoutManager(this)
        items.add(ScheduleAdapter.ViewHolder(LayoutInflater.from(this@BuatJadwalActivity).inflate(R.layout.item_list_schedule, rvJadwal, false)))
        rvJadwal.adapter = ScheduleAdapter(this, items)

        btn_back.setOnClickListener {
            onBackPressed()
        }

        btn_add.setOnClickListener {
            items.add(ScheduleAdapter.ViewHolder(LayoutInflater.from(this@BuatJadwalActivity).inflate(R.layout.item_list_schedule, rvJadwal, false)))
            (rvJadwal.adapter as ScheduleAdapter).notifyDataSetChanged()
        }

        bottom_btn.setOnClickListener {
            if (isNetworkConnected()) {
                val progressDialog = Progress(this)
                val place: ArrayList<String> = arrayListOf()
                val tanggal: ArrayList<String> = arrayListOf()
                val jam: ArrayList<String> = arrayListOf()
                val getItemRegistrationBatch = spinner_registration_id.selectedItem as RegistrationBatch.Data
                progressDialog.light("Loading")
                for (i in 0 until rvJadwal.childCount) {
                    place.add(rvJadwal.getChildAt(i).edit_text_place.text.toString())
                    tanggal.add(rvJadwal.getChildAt(i).edit_text_tanggal_ujian.text.toString())
                    jam.add(rvJadwal.getChildAt(i).edit_text_jam_ujian.text.toString())
                }

                getAPI().createSchedule(getItemRegistrationBatch.regist_time_id.toString(),
                                        place.toTypedArray(),
                                        tanggal.toTypedArray(),
                                        jam.toTypedArray())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            createSchedule: CreateSchedule? ->
                            progressDialog.dismiss()
                            when(createSchedule?.code) {
                                "success" -> {
                                    Toast.makeText(this@BuatJadwalActivity, createSchedule.message, Toast.LENGTH_SHORT).show()
                                    onBackPressed()
                                }
                                "failed" -> Toast.makeText(this@BuatJadwalActivity, createSchedule.message, Toast.LENGTH_SHORT).show()
                                else -> Log.e("Error", createSchedule?.message)
                            }
                        }, {
                            error -> Log.e("Error", error.message)
                        })
            }
        }
    }

    @SuppressLint("CheckResult")
    private fun getRegistrationBatch() {
        getAPI().getListRegistrationBatch()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({ registrationBatch: RegistrationBatch? ->
                    when(registrationBatch?.code) {
                        "success" -> {
                            val adapter: ArrayAdapter<RegistrationBatch.Data> = ArrayAdapter(this@BuatJadwalActivity, android.R.layout.simple_spinner_item, registrationBatch.data)
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                            spinner_registration_id.adapter = adapter
                        }
                        "failed" -> Toast.makeText(this@BuatJadwalActivity, registrationBatch.message, Toast.LENGTH_SHORT).show()
                        else -> Log.e("Error", registrationBatch?.message)
                    }}, {
                        error -> Log.e("Error", error.message)
                    })

    }
}
