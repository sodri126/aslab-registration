package ws.wolfsoft.baystudio.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import kotlinx.android.synthetic.main.activity_home.*
import ws.wolfsoft.aslabregistration.base.BaseActivity

import ws.wolfsoft.baystudio.R
import ws.wolfsoft.baystudio.fragment.*

class HomeActivity : BaseActivity() {
    override fun getLayoutId(): Int {
        return R.layout.activity_home
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        allEventActivities()
    }

    private fun allEventActivities() {
        navigation.setOnNavigationItemSelectedListener { item ->
            var selectedFragment: Fragment? = null
            when (item.itemId) {
                R.id.action_item1 -> selectedFragment = HomeFragment()
                R.id.action_item2 -> selectedFragment = JadwalFragment()
                R.id.action_item3 -> selectedFragment = NilaiFragment()
            }
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.frame_layout, selectedFragment!!)
            transaction.commit()
            true
        }

        //Manually displaying the first ws.wolfsoft.baystudio.fragment - one time only
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_layout, HomeFragment())
        transaction.commit()

        //Used to select an item programmatically
        //bottomNavigationView.getMenu().getItem(2).setChecked(true);
    }
}