package ws.wolfsoft.baystudio.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View

import com.github.mikephil.charting.charts.LineChart

import ws.wolfsoft.baystudio.R

class ReportActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report)
        val chart = findViewById<View>(R.id.chart) as LineChart
    }

    fun btnBack(view: View) {
        onBackPressed()
    }
}
