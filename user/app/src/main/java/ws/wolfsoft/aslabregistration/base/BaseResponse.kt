package ws.wolfsoft.aslabregistration.base

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class BaseResponse {
    @Expose
    @SerializedName("code")
    lateinit var code: String

    @Expose
    @SerializedName("message")
    lateinit var message: String
}
