package ws.wolfsoft.aslabregistration.model

import ws.wolfsoft.aslabregistration.R

enum class PeriodePendaftaran constructor(private val mTitleBatch: Int, private val mDateBegin: Int, private val mDateEnd: Int, private val mLayoutResId: Int) {

    FIRST(R.string.title_batch_one, R.string.daftar_date_begin_one, R.string.daftar_date_end_one, R.layout.item_periode_pendaftaran_one),
    SECOND(R.string.title_batch_two, R.string.daftar_date_begin_two, R.string.daftar_date_end_two, R.layout.item_periode_pendaftaran_two),
    THIRD(R.string.title_batch_three, R.string.daftar_date_begin_three, R.string.daftar_date_end_three, R.layout.item_periode_pendaftaran_three);
    fun getmTitleBatch(): Int {
        return mTitleBatch
    }

    fun getmDateBegin(): Int {
        return mDateBegin
    }

    fun getDateEnd(): Int {
        return mDateEnd
    }

    fun getmLayoutResId(): Int {
        return mLayoutResId
    }


}
