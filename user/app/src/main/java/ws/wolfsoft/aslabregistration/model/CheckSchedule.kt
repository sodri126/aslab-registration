package ws.wolfsoft.aslabregistration.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import ws.wolfsoft.aslabregistration.base.BaseResponse
import java.util.Date

class CheckSchedule : BaseResponse() {
    @Expose
    @SerializedName("data")
    lateinit var data: Data

    inner class Data {
        @Expose
        @SerializedName("schedule_id")
        var schedule_id: Int? = null

        @Expose
        @SerializedName("test_datetime")
        lateinit var test_datetime: Date

        @Expose
        @SerializedName("place")
        lateinit var place: String

        @Expose
        @SerializedName("total")
        var total: Int? = null
    }
}