package ws.wolfsoft.aslabregistration.activity

import android.content.Intent
import android.os.Bundle
import ws.wolfsoft.aslabregistration.R
import ws.wolfsoft.aslabregistration.base.BaseActivity
import kotlinx.android.synthetic.main.activity_success_registrasi.*

class SuccessRegistrationActivity: BaseActivity() {
    override fun getLayoutId(): Int {
        return R.layout.activity_success_registrasi
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        allActivityEvents()
    }

    private fun allActivityEvents() {
        btn_home.setOnClickListener{
            val intent = Intent(this@SuccessRegistrationActivity, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

}