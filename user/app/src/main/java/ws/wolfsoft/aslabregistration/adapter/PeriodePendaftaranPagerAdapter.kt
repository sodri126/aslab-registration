package ws.wolfsoft.aslabregistration.adapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import ws.wolfsoft.aslabregistration.model.PeriodePendaftaran

class PeriodePendaftaranPagerAdapter(private val mContext: Context) : PagerAdapter() {

    override fun instantiateItem(collection: ViewGroup, position: Int): Any {
        val periodePendaftaran = PeriodePendaftaran.values()[position]
        val inflater = LayoutInflater.from(mContext)
        val layout = inflater.inflate(periodePendaftaran.getmLayoutResId(), collection, false) as ViewGroup
        collection.addView(layout)

        return layout

    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    override fun getCount(): Int {
        return PeriodePendaftaran.values().size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getPageTitle(position: Int): CharSequence? {
        val customPagerEnum = PeriodePendaftaran.values()[position]
        return mContext.getString(customPagerEnum.getmTitleBatch())
    }

}
