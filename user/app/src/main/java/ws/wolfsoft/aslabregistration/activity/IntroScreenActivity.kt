package ws.wolfsoft.aslabregistration.activity

import android.content.Intent
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

import com.pixelcan.inkpageindicator.InkPageIndicator

import ws.wolfsoft.aslabregistration.adapter.IntroPagerAdapter
import ws.wolfsoft.aslabregistration.R

class IntroScreenActivity : AppCompatActivity() {

    private lateinit var btnBottom: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro_screen)

        btnBottom = findViewById(R.id.bottom_btn)
        btnBottom.setOnClickListener {

            val i = Intent(this@IntroScreenActivity, HomeActivity::class.java)
            this@IntroScreenActivity.startActivity(i)
            finish()
        }


        val viewPager = findViewById<ViewPager>(R.id.viewPager)
        viewPager.adapter = IntroPagerAdapter(this)

        val inkPageIndicator = findViewById<InkPageIndicator>(R.id.indicator)
        inkPageIndicator.setViewPager(viewPager)

    }


}
