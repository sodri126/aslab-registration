package ws.wolfsoft.aslabregistration.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import io.github.pierry.progress.Progress
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

import ws.wolfsoft.aslabregistration.R
import ws.wolfsoft.aslabregistration.base.BaseActivity
import kotlinx.android.synthetic.main.activity_cek_data.*
import ws.wolfsoft.aslabregistration.model.CheckRegistration

class CekDataActivity : BaseActivity() {

    override fun getLayoutId(): Int {
        return R.layout.activity_cek_data
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        allActivityEvents()
    }

    private fun allActivityEvents() {
        bottom_btn.setOnClickListener{
            if (isNetworkConnected()) {
                val progressDialog = Progress(this)
                progressDialog.light("Loading")
                getAPI().checkRegistration(edit_nim.text.toString())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe (
                                { checkRegistration: CheckRegistration? ->
                                    progressDialog.dismiss()
                                    when (checkRegistration?.code) {
                                        "success" -> {
                                            container_detail_registrasi.visibility = View.VISIBLE
                                            changeData(checkRegistration)
                                        }
                                        "failed" -> {
                                            container_detail_registrasi.visibility = View.INVISIBLE
                                            Toast.makeText(this@CekDataActivity, checkRegistration.message, Toast.LENGTH_SHORT).show()
                                        }
                                        else -> {
                                            container_detail_registrasi.visibility = View.INVISIBLE
                                            Log.e("Error", checkRegistration?.message)
                                        }
                                    }
                                },
                                { error ->
                                    progressDialog.dismiss()
                                    Log.e("Error", error.message) })
            }

        }


        btn_back.setOnClickListener {
            onBackPressed()
        }
    }

    private fun changeData(checkRegistration: CheckRegistration?) {
        container_detail_registrasi.visibility = View.VISIBLE
        txt_name.text = checkRegistration?.data?.nama
        txt_email.text = checkRegistration?.data?.email
        txt_jurusan.text = checkRegistration?.data?.major
        txt_tahun_ajaran.text = checkRegistration?.data?.year
        txt_nim.text = checkRegistration?.data?.nim
    }

}
