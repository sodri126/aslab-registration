package ws.wolfsoft.aslabregistration.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.RadioButton
import android.widget.Toast
import io.github.pierry.progress.Progress
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_registration.*

import ws.wolfsoft.aslabregistration.R
import ws.wolfsoft.aslabregistration.base.BaseActivity
import ws.wolfsoft.aslabregistration.model.CheckRegistration
import ws.wolfsoft.aslabregistration.model.Registration

class RegistrationActivity : BaseActivity() {
    override fun getLayoutId(): Int {
        return R.layout.activity_registration
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        allActivityEvents()
    }

    private fun allActivityEvents() {
        btn_check_nim.setOnClickListener {
            if (isNetworkConnected()) {
                val progressDialog = Progress(this)
                progressDialog.light("Loading")
                getAPI().checkRegistration(edit_text_nim.text.toString())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe (
                                { checkRegistration: CheckRegistration? ->
                                    progressDialog.dismiss()
                                    when (checkRegistration?.code) {
                                        "success" -> {
                                            changeData(checkRegistration)
                                        }
                                        "failed" -> {
                                            Toast.makeText(this@RegistrationActivity, checkRegistration.message, Toast.LENGTH_SHORT).show()
                                        }
                                        else -> {
                                            Log.e("Error", checkRegistration?.message)
                                        }
                                    }
                                },
                                { error ->
                                    progressDialog.dismiss()
                                    Log.e("Error", error.message) })
            }
        }
        btn_back.setOnClickListener {
            onBackPressed()
        }

        bottom_btn.setOnClickListener {
            if (isNetworkConnected()) {
                val progressDialog = Progress(this)
                progressDialog.light("Loading")

                val gender: String = if (input_group_radio_gender.checkedRadioButtonId != -1) findViewById<RadioButton>(input_group_radio_gender.checkedRadioButtonId).text.toString() else ""
                val major: String = if (input_group_radio_jurusan.checkedRadioButtonId != -1) findViewById<RadioButton>(input_group_radio_jurusan.checkedRadioButtonId).text.toString() else ""
                getAPI().processRegistration(edit_text_nim.text.toString(),
                        edit_text_nama_lengkap.text.toString(),
                        gender,
                        edit_text_tahun_ajaran.text.toString(),
                        major,
                        edit_text_email.text.toString(),
                        edit_text_nomor_telephone.text.toString())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            registration: Registration? ->
                            progressDialog.dismiss()
                            when(registration?.code) {
                                "success" -> {
                                    startActivity(Intent(this@RegistrationActivity, SuccessRegistrationActivity::class.java))
                                    finish()
                                }
                                "failed" -> {
                                    Toast.makeText(this@RegistrationActivity, registration.message, Toast.LENGTH_SHORT).show()
                                }
                                else -> {
                                    Log.e("ERROR", registration?.message)
                                }
                            }
                        }, {
                            error ->
                            progressDialog.dismiss()
                            Log.e("Error", error.message)
                        })
            }
        }
    }

    private fun changeData(checkRegistration: CheckRegistration?) {
        edit_text_nama_lengkap.setText(checkRegistration?.data?.nama)
        edit_text_email.setText(checkRegistration?.data?.email)
        edit_text_tahun_ajaran.setText(checkRegistration?.data?.year)
        edit_text_nomor_telephone.setText(checkRegistration?.data?.phone_number)

        if (checkRegistration?.data?.gender.equals("Laki-laki")) {
            lakilaki.isChecked = true
            perempuan.isChecked = false
        }
        else {
            perempuan.isChecked = true
            lakilaki.isChecked = false
        }

        if (checkRegistration?.data?.major.equals("Sistem Informasi")) {
            ti.isChecked = false
            si.isChecked = true
        } else {
            ti.isChecked = true
            si.isChecked = false
        }
//        txt_jurusan.text = checkRegistration?.data?.major
//        txt_tahun_ajaran.text = checkRegistration?.data?.year
//        txt_nim.text = checkRegistration?.data?.nim
    }

}
