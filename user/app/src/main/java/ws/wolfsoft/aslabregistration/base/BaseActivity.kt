package ws.wolfsoft.aslabregistration.base

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import ws.wolfsoft.aslabregistration.component.APIAslabRegistrationUser

abstract class BaseActivity : AppCompatActivity() {

    @LayoutRes
    abstract fun getLayoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
    }

    protected fun isNetworkConnected() : Boolean {
        val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return if(networkInfo != null && networkInfo.isConnected) true
        else  {
            Toast.makeText(this, "Tidak ada koneksi internet!", Toast.LENGTH_SHORT).show()
            false
        }
    }

    protected fun getAPI(): APIAslabRegistrationUser {
        return APIAslabRegistrationUser.create()
    }

//    override fun backToNavigate() {
//        onBackPressed()
//    }


}