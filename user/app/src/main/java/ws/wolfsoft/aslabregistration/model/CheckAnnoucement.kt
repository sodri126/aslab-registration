package ws.wolfsoft.aslabregistration.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import ws.wolfsoft.aslabregistration.base.BaseResponse

open class CheckAnnoucement : BaseResponse() {
    @Expose
    @SerializedName("data")
    lateinit var data: Data

    inner class Data {

        @Expose
        @SerializedName("nim")
        lateinit var nim: String

        @Expose
        @SerializedName("nama")
        lateinit var nama: String

        @Expose
        @SerializedName("score")
        var score: Int? = null

    }
}
