package ws.wolfsoft.aslabregistration.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.widget.CardView
import android.util.Log
import android.widget.LinearLayout
import android.widget.Toast
import com.pixelcan.inkpageindicator.InkPageIndicator
import io.github.pierry.progress.Progress
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_home.*
import ws.wolfsoft.aslabregistration.R
import ws.wolfsoft.aslabregistration.adapter.PeriodePendaftaranPagerAdapter
import ws.wolfsoft.aslabregistration.base.BaseActivity
import ws.wolfsoft.aslabregistration.model.CheckRegistration
import ws.wolfsoft.aslabregistration.model.CheckSchedule
import ws.wolfsoft.aslabregistration.model.RegistrationTime

class HomeActivity : BaseActivity(){
    override fun getLayoutId(): Int {
        return R.layout.activity_home
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        allActivityEvents()

        val viewPager = findViewById<ViewPager>(R.id.periodeViewPager)
        viewPager.adapter = PeriodePendaftaranPagerAdapter(this)

        val inkPageIndicator = findViewById<InkPageIndicator>(R.id.indicator)
        inkPageIndicator.setViewPager(viewPager)

    }

    private fun allActivityEvents() {
        periodeViewPager.adapter = PeriodePendaftaranPagerAdapter(this)
        indicator.setViewPager(periodeViewPager)

        btnRegistrasi.setOnClickListener {
            if (isNetworkConnected()) {
                val progressDialog = Progress(this)
                progressDialog.light("Loading")

                getAPI().checkRegistrationTime()
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe (
                                { registrationTime: RegistrationTime? ->
                                    progressDialog.dismiss()
                                    when (registrationTime?.code) {
                                        "success" -> {
                                            if (registrationTime.data.max_regist!!-1 <= 0) {
                                                Toast.makeText(this, "Maaf anda tidak bisa " +
                                                        "pada ${registrationTime.data.description} sekarang, " +
                                                        "silahkan menunggu di batch selanjutnya"
                                                        , Toast.LENGTH_SHORT).show()
                                            }
                                            else
                                                startActivity(Intent(this, RegistrationActivity::class.java))
                                        }
                                        "failed" -> Toast.makeText(this, registrationTime.message, Toast.LENGTH_SHORT).show()
                                        else -> {
                                            Log.e("Error", registrationTime?.message)
                                        }
                                    }
                                },
                                { error ->
                                    progressDialog.dismiss()
                                    Log.e("Error", error.message) })
            }
        }

        btnCekRegistrasi.setOnClickListener{
            startActivity(Intent(this@HomeActivity, CekDataActivity::class.java))
        }

        btnCekJadwal.setOnClickListener {
            startActivity(Intent(this@HomeActivity, JadwalUjianActivity::class.java))
        }

        btnCekPengumuman.setOnClickListener {
            startActivity(Intent(this@HomeActivity, CekPengumumanActivity::class.java))
        }
    }




}


