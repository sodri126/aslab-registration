package ws.wolfsoft.aslabregistration.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import ws.wolfsoft.aslabregistration.base.BaseResponse

class CheckRegistration : BaseResponse() {
    @Expose
    @SerializedName("data")
    lateinit var data: Data

    inner class Data {

        @Expose
        @SerializedName("nim")
        lateinit var nim: String

        @Expose
        @SerializedName("nama")
        lateinit var nama: String

        @Expose
        @SerializedName("gender")
        lateinit var gender: String

        @Expose
        @SerializedName("year")
        lateinit var year: String

        @Expose
        @SerializedName("major")
        lateinit var major: String

        @Expose
        @SerializedName("email")
        lateinit var email: String

        @Expose
        @SerializedName("phone_number")
        lateinit var phone_number: String

    }
}