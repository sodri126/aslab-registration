package ws.wolfsoft.aslabregistration.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import io.github.pierry.progress.Progress
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ws.wolfsoft.aslabregistration.R
import ws.wolfsoft.aslabregistration.base.BaseActivity
import kotlinx.android.synthetic.main.activity_cek_pengumuman.*
import ws.wolfsoft.aslabregistration.model.CheckAnnoucement
import ws.wolfsoft.aslabregistration.model.SendResultAnnoucement

class CekPengumumanActivity : BaseActivity() {

    override fun getLayoutId(): Int {
        return R.layout.activity_cek_pengumuman
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        runActivity()

    }

    private fun runActivity() {
        //btn back
        btn_back.setOnClickListener{
            onBackPressed()
        }

        btnPrintHasil.setOnClickListener{
            if (isNetworkConnected()) {
                val progressDialog = Progress(this)
                progressDialog.light("Loading")
                getAPI().sendResultAnnoucement(edit_text_registration_id.text.toString())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe (
                                { sendResultAnnoucement: SendResultAnnoucement? ->
                                    progressDialog.dismiss()
                                    Toast.makeText(this@CekPengumumanActivity, sendResultAnnoucement?.message, Toast.LENGTH_SHORT).show()
                                },
                                { error ->
                                    container_pengumuman.visibility = View.INVISIBLE
                                    progressDialog.dismiss()
                                    Log.e("ERROR", error.message)
                                })

            }
        }
        // btn check
        bottom_btn.setOnClickListener {
            if (isNetworkConnected()) {
                val progressDialog = Progress(this)
                progressDialog.light("Loading")
                getAPI().checkAnnoucement(edit_text_registration_id.text.toString())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe (
                                { annoucement: CheckAnnoucement? ->
                                    progressDialog.dismiss()
                                    when (annoucement?.code) {
                                        "success" -> {
                                            changeData(annoucement)
                                        }
                                        "failed" -> {
                                            container_pengumuman.visibility = View.INVISIBLE
                                            Toast.makeText(this@CekPengumumanActivity, annoucement.message, Toast.LENGTH_SHORT).show()
                                        }
                                        else -> {
                                            container_pengumuman.visibility = View.INVISIBLE
                                            Log.e("ERROR", annoucement?.message)
                                        }
                                    }
                                },
                                { error ->
                                    container_pengumuman.visibility = View.INVISIBLE
                                    progressDialog.dismiss()
                                    Log.e("ERROR", error.message)
                                })

            }
        }
    }

    private fun changeData(annoucement: CheckAnnoucement?) {
        container_pengumuman.visibility = View.VISIBLE
        txt_name.text = annoucement?.data?.nama
        txt_nim.text = annoucement?.data?.nim
        txt_status.text = if(annoucement?.data?.score!! > 75) "LULUS" else "TIDAK LULUS"
        txt_status.setTextColor(resources.getColor(R.color.txt_color))
    }


}
