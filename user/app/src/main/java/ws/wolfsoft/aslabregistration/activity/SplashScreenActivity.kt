package ws.wolfsoft.aslabregistration.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import ws.wolfsoft.aslabregistration.R

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)


        val background = object : Thread() {
            override fun run() {

                try {
                    // Thread will sleep for 5 seconds
                    Thread.sleep((2 * 1000).toLong())
                    val intent = Intent(this@SplashScreenActivity, IntroScreenActivity::class.java)
                    startActivity(intent)
                    // After 5 seconds redirect to another intent
                    //Remove activity
                    finish()
                } catch (e: Exception) {

                }

            }
        }
        // start thread
        background.start()


    }

}
