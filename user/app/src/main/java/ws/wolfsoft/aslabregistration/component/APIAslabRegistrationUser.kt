package ws.wolfsoft.aslabregistration.component

import com.google.gson.GsonBuilder
import io.reactivex.Single
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import ws.wolfsoft.aslabregistration.BuildConfig
import ws.wolfsoft.aslabregistration.model.*
import java.net.CookieManager
import java.util.concurrent.TimeUnit

interface APIAslabRegistrationUser {


    // User
    @POST("check-schedule.php")
    @FormUrlEncoded
    fun checkSchedule(@Field("registrationId") registrationId: String): Single<CheckSchedule>

    @POST("check-annoucement.php")
    @FormUrlEncoded
    fun checkAnnoucement(@Field("registrationId") registrationId: String): Single<CheckAnnoucement>

    @POST("check-registration.php")
    @FormUrlEncoded
    fun checkRegistration(@Field("nim") nim: String): Single<CheckRegistration>

    @POST("registration.php")
    @FormUrlEncoded
    fun processRegistration(@Field("nim") nim: String,
                            @Field("nama") nama: String,
                            @Field("gender") gender: String,
                            @Field("year") year: String,
                            @Field("major") major: String,
                            @Field("email") email: String,
                            @Field("phone_number") phoneNumber: String): Single<Registration>

    @GET("check-registration-time.php")
    fun checkRegistrationTime(): Single<RegistrationTime>

    @POST("send-result-annoucement.php")
    @FormUrlEncoded
    fun sendResultAnnoucement(@Field("registrationId") registrationId: String): Single<SendResultAnnoucement>
    // User

    companion object Factory {
        fun create(): APIAslabRegistrationUser {
            val READ_TIMEOUT = 100 * 1000
            val CONNECTION_TIMEOUT = 100 * 1000

            val logger = HttpLoggingInterceptor()
            logger.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

            val okHttp =  OkHttpClient.Builder()
                    .cookieJar(JavaNetCookieJar(CookieManager()))
                    .retryOnConnectionFailure(false)
                    .readTimeout(READ_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
                    .connectTimeout(CONNECTION_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
                    .addInterceptor(logger)
                    .build()

            var gson = GsonBuilder()
                    .setDateFormat("yyyy-MM-dd HH:mm:ss")
                    .create()

            val retrofit = Retrofit.Builder()
                    .client(okHttp)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl("http://aslab.pdsinergi.com/api/")
                    .build()

            return retrofit.create(APIAslabRegistrationUser::class.java)
        }
    }
}