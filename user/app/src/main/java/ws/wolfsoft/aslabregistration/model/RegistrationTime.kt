package ws.wolfsoft.aslabregistration.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import ws.wolfsoft.aslabregistration.base.BaseResponse
import java.util.*

class RegistrationTime : BaseResponse() {
    @Expose
    @SerializedName("data")
    lateinit var data: Data

    inner class Data {
        @Expose
        @SerializedName("regist_time_id")
        var regist_time_id: Int? = null

        @Expose
        @SerializedName("open_date")
        lateinit var open_date: Date

        @Expose
        @SerializedName("close_date")
        lateinit var close_date: Date

        @Expose
        @SerializedName("description")
        lateinit var description: String

        @Expose
        @SerializedName("max_regist")
        var max_regist: Int? = null
    }
}