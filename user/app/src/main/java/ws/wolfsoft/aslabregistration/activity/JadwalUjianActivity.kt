package ws.wolfsoft.aslabregistration.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import io.github.pierry.progress.Progress
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_jadwal_ujian.*

import ws.wolfsoft.aslabregistration.R
import ws.wolfsoft.aslabregistration.base.BaseActivity
import ws.wolfsoft.aslabregistration.model.CheckSchedule
import java.text.SimpleDateFormat

class JadwalUjianActivity : BaseActivity() {
    override fun getLayoutId(): Int {
        return R.layout.activity_jadwal_ujian
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityEvents()
    }

    private fun activityEvents() {
        btn_back.setOnClickListener {
            onBackPressed()
        }

        bottom_btn.setOnClickListener{
            if (isNetworkConnected()) {
                val progressDialog = Progress(this)
                progressDialog.light("Loading")
                getAPI().checkSchedule(edit_text_registration_id.text.toString())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe (
                                { schedule: CheckSchedule? ->
                                    progressDialog.dismiss()
                                    when (schedule?.code) {
                                        "success" -> changeData(schedule)
                                        "failed" -> {
                                            container_detail_jadwal.visibility = View.INVISIBLE
                                            Toast.makeText(this@JadwalUjianActivity, schedule.message, Toast.LENGTH_SHORT).show()
                                        }
                                        "error" -> {
                                            container_detail_jadwal.visibility = View.INVISIBLE
                                            Log.e("ERROR", schedule.message)
                                        }
                                    }
                                },
                                { error ->
                                    container_detail_jadwal.visibility = View.INVISIBLE
                                    progressDialog.dismiss()
                                    Log.e("Error", error.message)
                                })
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun changeData(schedule: CheckSchedule?) {
        container_detail_jadwal.visibility = View.VISIBLE
        txt_registration_no.text = edit_text_registration_id.text
        txt_place.text = schedule?.data?.place
        txt_tanggal_ujian.text = SimpleDateFormat("dd MMM yyyy").format(schedule?.data?.test_datetime)
        txt_waktu_ujian.text = SimpleDateFormat("HH:mm").format(schedule?.data?.test_datetime)
        txt_total_siswa.text = "${schedule?.data?.total} Orang"
    }
}
